import Vue from 'vue'
import Vuex from 'vuex'

import authentication from "../store/modules/authentication"
import profile from "../store/modules/profile"
import modal from "../store/modules/modal"
import course from "../store/modules/course"
import topic from "../store/modules/topic"
import action from "../store/modules/action"
import user from "../store/modules/users"
import examList from "../store/modules/exam"
import examset from "../store/modules/examset"
import file from "../store/modules/file"
import questionType from "../store/modules/questionType"
import analyticReport from "../store/modules/analyticReport"

Vue.use(Vuex)

const state = {
    sidebarShow: 'responsive',
    sidebarMinimize: false,
    printPDF: false
}

const mutations = {
    toggleSidebarDesktop(state) {
        const sidebarOpened = [true, 'responsive'].includes(state.sidebarShow)
        state.sidebarShow = sidebarOpened ? false : 'responsive'
    },
    toggleSidebarMobile(state) {
        const sidebarClosed = [false, 'responsive'].includes(state.sidebarShow)
        state.sidebarShow = sidebarClosed ? true : 'responsive'
    },
    set(state, [variable, value]) {
        state[variable] = value
    },
    printPDF(state, status) {
        state.printPDF = status
    }
}

const getters = {
    printPDF(state) {
        return state.printPDF
    }
}

export default new Vuex.Store({
    state,
    mutations,
    getters,
    modules: {
        auth: authentication,
        modal,
        profile,
        course,
        action,
        topic,
        user,
        exam: examList,
        examset,
        file,
        questionType,
        analyticReport
    }
})

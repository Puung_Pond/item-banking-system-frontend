import Service from "../../service/api"
import store from "@/store/store";
import error from "@/service/error";

const CourseModule = {
    namespaced: true,
    state: {
        topics: []
    },
    mutations: {
        onSetTopics(state, topics) {
            state.topics = topics
        }
    },
    actions: {
        onCreateTopicName({commit}, body) {
            store.commit("modal/onSetWaitStatus")
            Service.onCreateTopic(body)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                        this.dispatch("topic/onSetTopics")
                    }, 500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
        },
        onSetTopics({commit}) {
            Service.onFetchAllTopic()
                .then(res => {
                    commit("onSetTopics", res.data.data)
                })
                .catch(err => {
                    error.errorHandler(err.response.status)
                })
        },
        onEditTopicName({commit}, value) {
            store.commit("modal/onSetWaitStatus")
            Service.onEditTopic(value.topicId, {name: value.name})
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                        this.dispatch("topic/onSetTopics")
                    }, 1000)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
        },
        onDeleteTopic({commit}, body) {
            store.commit("modal/onSetConfirmActionModalStatus", {status: false})
            store.commit("modal/onSetWaitStatus")
            Service.onDeleteTopic(body)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                        this.dispatch("topic/onSetTopics")
                    }, 500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
            store.commit("action/onSetActionStatus", false)
        }
    },
    getters: {
        getTopics(state) {
            return state.topics
        }
    }
}

export default CourseModule

import Service from "../../service/api"
import store from "../store"
import error from "@/service/error";

const ExamSetModule = {
    namespaced: true,
    state: {
        examsets: [],
        analysisResults: {},
        currentExamSet: [],
        singleExamset: {}
    },
    mutations: {
        onSetExamSets(state, examsets) {
            state.examsets = examsets
        },
        onSetSingleExamSet(state, data) {
            state.singleExamset = data
        },
        onSetAnalysisResult(state, results) {
            state.analysisResults = results
        },
        onAddCurrentExamSet(state, examSet) {
            state.currentExamSet = examSet
        }
    },
    actions: {
        onSetExamSets({commit}) {
            Service.onFetchAllExamSet()
                .then(res => {
                    let body = []
                    for (let i = 0; i < res.data.data.length; i++) {
                        let payload = {
                            examSetId: res.data.data[i]._id,
                            examSetCode: res.data.data[i].code,
                            examSetName: res.data.data[i].name,
                            examSetNumberOfUse: res.data.data[i].numberOfUse,
                            examSetItems: res.data.data[i].exam
                        }
                        body.push(payload)
                    }
                    commit("onSetExamSets", body)
                })
                .catch(err => {
                    error.errorHandler(err.response.status)
                })
        },
        onGetSingleExamset({commit}, id) {
            store.commit("modal/onSetWaitStatus")
            Service.onGetSingleExamset(id)
                .then(res => {
                    commit("onSetSingleExamSet", res.data.data)
                    store.commit("modal/onDeleteStatus")
                })
                .catch(err => {
                    error.errorHandler(err.response.status)
                })
        },
        onExamsetAnalysisThenSave({commit}, body) {
            store.commit("modal/onSetWaitStatus")
            const formData = new FormData();
            formData.append('file', body.file);
            Service.onExamsetAnalysisThenSave(formData)
                .then((res) => {
                    store.commit("modal/onSetSuccessStatus")
                    store.commit("examset/onSetAnalysisResult", res.data.data)
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                    }, 500)
                })
                .catch((err) => {
                    error.errorHandler(err.response.status)
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                })
        },
        onExamsetAnalysisThenNotSave({commit}, body) {
            store.commit("modal/onSetWaitStatus")
            const formData = new FormData();
            formData.append('file', body.file);
            Service.onExamsetAnalysisThenNotSave(formData)
                .then((res) => {
                    store.commit("modal/onSetSuccessStatus")
                    store.commit("examset/onSetAnalysisResult", res.data.data)
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                    }, 500)
                })
                .catch((err) => {
                    error.errorHandler(err.response.status)
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                })
        },
        onDeleteExamset({commit}, id) {
            store.commit("modal/onSetWaitStatus")
            Service.onDeleteExamset(id)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    store.dispatch("examset/onSetExamSets")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                    }, 500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
        },
        onSetSingleExamSets({commit}, url) {
            let examSets = []
            for (let i = 0; i < url.length; i++) {
                Service.onFetchSingleExamSet(url[i])
                    .then((res) => {
                        examSets.push(res.data.data)
                    })
                    .catch(err => error.errorHandler(err.response.status))
            }
            commit("onAddCurrentExamSet", examSets)
        }
    },
    getters: {
        getExamSets(state) {
            return state.examsets
        },
        getAnalysisResult(state) {
            return state.analysisResults
        },
        getCurrentExamSet(state) {
            return state.currentExamSet
        },
        getSingleExamset(state) {
            return state.singleExamset
        }
    }
}
export default ExamSetModule

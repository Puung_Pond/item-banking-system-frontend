import Service from "../../service/api"
import store from "@/store/store";
import error from "@/service/error";

const CourseModule = {
    namespaced: true,

    state: {
        questionTypes: [],
    },

    mutations: {
        onSetQuestionTypes(state, questionTypes) {
            state.questionTypes = questionTypes
        }
    },

    actions: {
        onSetQuestionTypes({commit}) {
            Service.onFetchAllQuestionTypes()
                .then(res => {
                    commit("onSetQuestionTypes", res.data.data)
                })
                .catch(err => {
                    error.errorHandler(err.response.status)
                })
        },
        onAddQuestionType({commit, dispatch}, body) {
            store.commit("modal/onSetWaitStatus")
            Service.onCreateQuestionType(body)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                        dispatch("onSetQuestionTypes")
                    }, 500)
                })
                .catch((err) => {
                    error.errorHandler(err.response.status)
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                })
        },
        onEditQuestionType({commit, dispatch}, body) {
            store.commit("modal/onSetWaitStatus")
            Service.onEditQuestionType(body._id, body)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                        dispatch("onSetQuestionTypes")
                    }, 500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
        },
        onDeleteQuestionType({commit, dispatch}, id) {
            store.commit("modal/onSetConfirmActionModalStatus", {status: false})
            store.commit("modal/onSetWaitStatus")
            Service.onDeleteQuestionType(id)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                        dispatch("onSetQuestionTypes")
                    }, 500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
            store.commit("action/onSetActionStatus", false)
        }
    },

    getters: {
        questionTypes(state) {
            return state.questionTypes
        }
    }
}

export default CourseModule

import Service from "../../service/api";
import error from "@/service/error";

const ProfileModule = {
    namespaced: true,

    state: {
        userInformation: {},
    },

    mutations: {
        onSetUserInformation(state, value) {
            state.userInformation = value
        }
    },

    actions: {
        onFetchUserInformation({commit}) {
            Service.onFetchUserInformation()
                .then(res => {
                    commit("onSetUserInformation", res.data.data)
                })
                .catch(err => {
                    error.errorHandler(err.response.status)
                })
        },
    },

    getters: {
        userInformation(state) {
            return state.userInformation;
        }
    },
};

export default ProfileModule;

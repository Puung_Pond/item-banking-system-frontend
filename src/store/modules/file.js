import Service from "../../service/api"
import store from "@/store/store";
import error from "@/service/error";

const FileModule = {
    namespaced: true,
    state: {
        files: []
    },
    mutations: {
        onSetFiles(state, files) {
            state.files = files
        }
    },
    actions: {
        onSendFile({commit}, body) {
            store.commit("modal/onSetWaitStatus")
            const formData = new FormData()
            formData.append("file", body.file)
            if (body.receiver) formData.append("receiver", body.receiver)
            formData.append("description", body.description)
            Service.onSendFile(formData)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    store.dispatch("file/onSetFiles")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                    }, 1500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
        },
        onSetFiles({commit}) {
            Service.onFetchAllFile()
                .then((res) => {
                    const response = res.data.data
                    let body = []
                    for (let i = 0; i < response.length; i++) {
                        let date = new Date(Date.parse(response[i].createdAt));
                        let payload = {
                            createdAt: date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear(),
                            fileId: response[i]._id,
                            fileDescription: response[i].description,
                            fileName: response[i].name,
                            fileSender: response[i].sender.firstName + " " + response[i].sender.lastName,
                            fileUrl: response[i].url,
                            isReceived: response[i].isReceived,
                            fileSenderId: response[i].sender._id,
                        }
                        if (response[i].receiver) {
                            payload.fileReceiverId = response[i].receiver._id
                        }
                        if (response[i].receiver !== undefined) {
                            payload.fileReceiver = response[i].receiver.firstName + " " + response[i].receiver.lastName
                        }
                        body.push(payload)
                    }
                    commit("onSetFiles", body)
                })
                .catch((err) => {
                    error.errorHandler(err.response.status)
                })
        },
        onAcceptFile({commit}, body) {
            store.commit("modal/onSetWaitStatus")
            let payload = {}
            if (body.receiver) payload.receiver = body.receiver
            payload.isReceived = body.isReceived
            Service.onAcceptFile(body.fileId, payload)
                .then(() => {
                    store.dispatch("file/onSetFiles")
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                    }, 1500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
        },
        onDeleteFile({commit}, body) {
            store.commit("modal/onSetWaitStatus")
            Service.onDeleteFile(body)
                .then(() => {
                    store.dispatch("file/onSetFiles")
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                    }, 1500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
        }
    },
    getters: {
        getFiles(state) {
            return state.files
        }
    }
}

export default FileModule

const ActionModule = {
    namespaced: true,
    state: {
        actionStatus: false
    },

    mutations: {
        onSetActionStatus(state, value) {
            state.actionStatus = value
        }
    },

    getters: {
        actionStatus(state) {
            return state.actionStatus;
        }
    },
};

export default ActionModule;

import Service from "../../service/api"
import InspectFormData from "../../service/inspectFormData"
import store from "@/store/store";
import error from "@/service/error";

const ExamModule = {
    namespaced: true,
    state: {
        exams: []
    },
    mutations: {
        onSetExams(state, topics) {
            state.exams = topics
        }
    },
    actions: {
        onSetExams({commit}) {
            Service.onFetchAllExam()
                .then(res => {
                    let body = []
                    for (let i = 0; i < res.data.data.length; i++) {
                        let payload = {
                            question: res.data.data[i].question,
                            questionTopic: res.data.data[i].topic,
                            questionType: res.data.data[i].questionType,
                            keyword: res.data.data[i].keyword,
                            quality: res.data.data[i].quality,
                            difficulty: res.data.data[i].difficulty,
                            numberOfUse: res.data.data[i].numberOfUse,
                        }
                        body.push(payload)
                    }
                    commit("onSetExams", body)
                })
                .catch(err => {
                    error.errorHandler(err.response.status)
                })
        },
        onRandomExamByTOS({commit, dispatch}, body) {
            for (let i = 0; i < body.tos.length; i++) {
                delete body.tos[i].courseName
                delete body.tos[i].topicName
                delete body.tos[i].userName
                body.tos[i].maxDifficultyIndex = parseFloat(body.tos[i].maxDifficultyIndex)
                body.tos[i].minDifficultyIndex = parseFloat(body.tos[i].minDifficultyIndex)
                body.tos[i].maxDiscriminationIndex = parseFloat(body.tos[i].maxDiscriminationIndex)
                body.tos[i].minDiscriminationIndex = parseFloat(body.tos[i].minDiscriminationIndex)

                if (!body.tos[i].maxDifficultyIndex) delete body.tos[i].maxDifficultyIndex
                if (!body.tos[i].minDifficultyIndex) delete body.tos[i].minDifficultyIndex
                if (!body.tos[i].minDiscriminationIndex) delete body.tos[i].minDiscriminationIndex
                if (!body.tos[i].maxDiscriminationIndex) delete body.tos[i].maxDiscriminationIndex

                if (body.tos[i].keyword.length === 0) delete body.tos[i].keyword
                if (body.tos[i].totalAmount) body.tos[i].totalAmount = parseInt(body.tos[i].totalAmount)
                if (body.tos[i].user === "") delete body.tos[i].user
                if (body.tos[i].topic === "") delete body.tos[i].topic
                if (body.tos[i].course === "") delete body.tos[i].course

                for (let j = body.tos[i].type.length - 1; j >= 0; j--) {
                    if (body.tos[i].type[j].name) delete body.tos[i].type[j].name
                    body.tos[i].type[j].amount = parseInt(body.tos[i].type[j].amount)
                    if (body.tos[i].type[j].amount === 0) {
                        body.tos[i].type.splice(j, 1);
                    }
                    if (j === 0) {
                        body.tos[i].type.length === 0 ? delete body.tos[i].type : null
                    }
                }
            }
            store.commit("modal/onSetWaitStatus")
            Service.onRandomExamByTOS(body)
                .then(res => {
                    store.dispatch("examset/onSetExamSets")
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                    }, 500)
                    // store.dispatch("examset/onSetSingleExamSets", res.data.data.exam)
                })
                .catch(err => {
                    error.errorHandler(err.response.status)
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                    }, 500)
                })
        },
        onAddExamToSystem({_, dispatch}, body) {
            store.commit("modal/onSetWaitStatus")
            body.forEach((element, index) => {
                const formData = new FormData()
                element.course ? formData.append("course", element.course) : delete element.course
                element.topic ? formData.append("topic", element.topic) : delete element.topic
                element.user ? formData.append("user", element.user) : delete element.user
                element.keyword ? formData.append('keyword', JSON.stringify(element.keyword)) : delete element.keyword
                element.question ? formData.append('question', element.question) : delete element.question
                element.questionType ? formData.append('questionType', element.questionType) : delete element.questionType
                element.difficultyIndex !== null ? formData.append('difficultyIndex', element.difficultyIndex) : delete element.difficultyIndex
                element.discriminationIndex !== null ? formData.append('discriminationIndex', element.discriminationIndex) : delete element.discriminationIndex

                if (element.answerType === "Photo") {
                    element.answers.forEach(function (item) {
                        delete item.answerText
                    })
                    formData.append('answerA', element.files.answerA);
                    formData.append('answerB', element.files.answerB);
                    formData.append('answerC', element.files.answerC);
                    formData.append('answerD', element.files.answerD);
                    formData.append('answerE', element.files.answerE);
                } else if (element.answerType === "Text") {
                    delete element.files.answerA
                    delete element.files.answerB
                    delete element.files.answerC
                    delete element.files.answerD
                    delete element.files.answerE
                }

                formData.append('answers', JSON.stringify(element.answers))

                element.files.questionPhoto ? element.files.questionPhoto.length === 0 ? delete element.files.questionPhoto :
                    element.files.questionPhoto.forEach(photo => {
                        formData.append("questionPhoto", photo)
                    }) : delete element.files.questionPhoto

                // InspectFormData.checkFormData(formData)

                Service.onAddExam(formData)
                    .then((res) => {
                        if (index === body.length - 1) {
                            dispatch("onSetExams")
                            store.commit("modal/onDeleteStatus")
                        }
                    })
                    .catch((err) => {
                        error.errorHandler(err.response.status)
                    })
            })
        }
    },
    getters: {
        getExams(state) {
            return state.exams
        }
    }
}

export default ExamModule

import Service from "../../service/api"
import store from "@/store/store";
import error from "@/service/error";

const CourseModule = {
    namespaced: true,

    state: {
        analyticReports: [],
    },

    mutations: {
        onSetAnalyticReports(state, analyticReports) {
            state.analyticReports = analyticReports
        }
    },

    actions: {
        onSetAnalyticReports({commit}) {
            Service.onFetchAllAnalyticReports()
                .then(res => {
                    commit("onSetAnalyticReports", res.data.data)
                })
                .catch(err => {
                    error.errorHandler(err.response.status)
                })
        },
        onDeleteAnalyticReports({commit, dispatch}, id) {
            store.commit("modal/onSetConfirmActionModalStatus", {status: false})
            store.commit("modal/onSetWaitStatus")
            Service.onDeleteAnalyticReports(id)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                        dispatch("onSetAnalyticReports")
                    }, 500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
            store.commit("action/onSetActionStatus", false)
        }
    },

    getters: {
        analyticReports(state) {
            return state.analyticReports
        }
    }
}

export default CourseModule

import Service from "../../service/api"
import store from "@/store/store";
import error from "@/service/error";

const UserModule = {
    namespaced: true,

    state: {
        users: [],
        dashboard: {}
    },

    mutations: {
        onSetUsers(state, users) {
            state.users = users
        },
        onSetDashboard(state, information) {
            state.dashboard = information
        }
    },

    actions: {
        onSetDashboard({commit}) {
            Service.onFetchDashboard()
                .then(res => {
                    commit("onSetDashboard", res.data.data)
                })
                .catch(err => {
                    error.errorHandler(err.response.status)
                })
        },
        onSetUsers({commit}) {
            Service.onFetchAllUser()
                .then(res => {
                    commit("onSetUsers", res.data.data)
                })
                .catch(err => {
                    error.errorHandler(err.response.status)
                })
        },
        onCreateUsers({commit}, body) {
            delete body.id
            store.commit("modal/onSetWaitStatus")
            Service.onCreateUser(body)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    this.dispatch("user/onSetUsers")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                    }, 700)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
        },
        onEditUsers({commit}, body) {
            store.commit("modal/onSetWaitStatus")
            Service.onEditUser(body)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    this.dispatch("user/onSetUsers")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                    }, 700)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
        },
        onResetUserPassword({commit}, body) {
            store.commit("modal/onSetWaitStatus")
            Service.onResetUserPassword(body)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                    }, 500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
        },
        onDeleteUser({commit}, userId) {
            store.commit("modal/onSetConfirmActionModalStatus", {status: false})
            store.commit("modal/onSetWaitStatus")
            Service.onDeleteUser(userId)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                        this.dispatch("user/onSetUsers")
                    }, 500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
            store.commit("action/onSetActionStatus", false)
        }
    },

    getters: {
        getUsers(state) {
            return state.users
        },
        getDashboard(state) {
            return state.dashboard
        }
    }
}

export default UserModule

import Service from "../../service/api";
import router from "../../router/index";
import store from "../store"
import error from "@/service/error";

const LoginModule = {
    namespaced: true,

    state: {
        userLoginStatus: false,
    },

    mutations: {
        onSetUserLoginStatus(state, value) {
            state.userLoginStatus = value
        },
        onClearUserData(state, value) {
            state.userLoginStatus = value
        }
    },

    actions: {
        onLogin({commit}, body) {
            store.commit("modal/onSetWaitStatus")
            Service.onLogin(body)
                .then((res) => {
                    router.push("mfu/dashboard")
                    store.commit("modal/onDeleteStatus")
                    commit("onSetUserLoginStatus", true)
                    localStorage.setItem("token", res.data.token)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                });
        },
        onLogout({commit}) {
            if (localStorage.getItem("token")) {
                Service.onLogout()
                    .then(res => {
                        commit("onClearUserData", false)
                        localStorage.clear()
                    })
                    .catch(err => error.errorHandler(err.response.status))
            }
        },
        onChangePassword({commit}, body) {
            store.commit("modal/onSetWaitStatus")
            Service.onUpdatePassword(body)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                        router.push("/")
                    }, 500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                }, 500)
        }
    },

    getters: {
        userLoginStatus(state) {
            return state.userLoginStatus;
        }
    },
};

export default LoginModule;

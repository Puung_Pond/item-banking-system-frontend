const ModalModule = {
    namespaced: true,
    state: {
        modalStatus: {
            title: "Notification",
            color: "info",
            status: false,
            text: "The system is processing. Just a moment, please."
        },
        confirmActionModalStatus: {
            title: "Notification",
            color: "warning",
            status: false,
            text: "The system is processing. Just a moment, please."
        }
    },

    mutations: {
        onSetWaitStatus(state) {
            state.modalStatus = {
                title: "Just a moment, please.",
                color: "info",
                status: true,
                text: "The request is being processed."
            }
        },
        onDeleteStatus(state) {
            state.modalStatus = {
                status: false
            }
        },
        onSetSuccessStatus(state) {
            state.modalStatus = {
                title: "Successful!",
                color: "success",
                status: true,
                text: "The system has processed the request successfully."
            }
        },
        onSetFailStatus(state, status) {
            state.modalStatus = {
                title: "Something went wrong!",
                color: "danger",
                status: true,
                text: status
            }
        },
        onSetModalStatus(state, value) {
            state.modalStatus = value
        },
        onSetConfirmActionModalStatus(state, value) {
            state.confirmActionModalStatus = value
        }
    },

    getters: {
        modalStatus(state) {
            return state.modalStatus;
        },
        confirmActionModalStatus(state) {
            return state.confirmActionModalStatus;
        }
    },
};

export default ModalModule;

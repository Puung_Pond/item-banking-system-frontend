import Service from "../../service/api"
import store from "@/store/store";
import error from "@/service/error";

const CourseModule = {
    namespaced: true,

    state: {
        courses: [],
        course: {}
    },

    mutations: {
        onSetCourses(state, courses) {
            state.courses = courses
        },
        onSetCourse(state, course) {
            state.course = course
        }
    },

    actions: {
        onSetCourses({commit}) {
            Service.onFetchAllCourses()
                .then(res => {
                    commit("onSetCourses", res.data.data)
                })
                .catch(err => {
                    error.errorHandler(err.response.status)
                })
        },
        onFetchSingleCourse({commit}, courseId) {
            store.commit("modal/onSetWaitStatus")
            Service.onFetchSingleCourse(courseId)
                .then(res => {
                    commit("onSetCourse", res.data.data)
                    store.commit("modal/onDeleteStatus")
                })
                .catch(err => {
                    error.errorHandler(err.response.status)
                    store.commit("modal/onDeleteStatus")
                })
        },
        onAddCourse({commit}, body) {
            store.commit("modal/onSetWaitStatus")
            Service.onCreateCourse(body)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                        store.dispatch("course/onSetCourses")
                    }, 500)
                })
                .catch((err) => {
                    error.errorHandler(err.response.status)
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                })
        },
        onEditCourse({commit}, body) {
            store.commit("modal/onSetWaitStatus")
            Service.onEditCourse(body._id, body)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                        store.dispatch("course/onSetCourses")
                    }, 500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
        },
        onDeleteCourse({commit}, body) {
            store.commit("modal/onSetConfirmActionModalStatus", {status: false})
            store.commit("modal/onSetWaitStatus")
            Service.onDeleteCourse(body)
                .then(() => {
                    store.commit("modal/onSetSuccessStatus")
                    setTimeout(() => {
                        store.commit("modal/onDeleteStatus")
                        this.dispatch("course/onSetCourses")
                    }, 500)
                })
                .catch((err) => {
                    store.commit("modal/onSetFailStatus", err.response.data.error)
                    error.errorHandler(err.response.status)
                })
            store.commit("action/onSetActionStatus", false)
        }
    },

    getters: {
        getCourses(state) {
            return state.courses
        },
        getCourse(state) {
            return state.course
        }
    }
}

export default CourseModule

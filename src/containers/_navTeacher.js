export default [
    {
        _name: 'CSidebarNav',
        _children: [
            {
                _name: 'CSidebarNavItem',
                name: 'Main Page',
                to: '/mfu/dashboard',
                icon: 'cil-bank',
                badge: {
                    color: 'success',
                    text: 'HOME'
                }
            },
            {
                _name: 'CSidebarNavTitle',
                _children: ['Report']
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Formula',
                to: '/mfu/formula',
                icon: 'cil-calculator'
            },
            {
                _name: 'CSidebarNavTitle',
                _children: ['file management']
            },
            {
                _name: 'CSidebarNavItem',
                name: 'File Transfer',
                to: '/mfu/file',
                icon: 'cil-file'
            },
            {
                _name: 'CSidebarNavTitle',
                _children: ['authentication']
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Change Password',
                to: '/mfu/changepassword',
                icon: 'cil-lock-locked'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Log out',
                to: '/login',
                icon: 'cil-account-logout'
            }
        ]
    }
]

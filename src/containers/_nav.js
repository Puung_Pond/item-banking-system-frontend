export default [
    {
        _name: 'CSidebarNav',
        _children: [
            {
                _name: 'CSidebarNavItem',
                name: 'Main Page',
                to: '/mfu/dashboard',
                icon: 'cil-bank',
                badge: {
                    color: 'success',
                    text: 'HOME'
                }
            },
            {
                _name: 'CSidebarNavTitle',
                _children: ['report']
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Analytic Result',
                to: '/mfu/report',
                icon: 'cil-file'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Formula',
                to: '/mfu/formula',
                icon: 'cil-calculator'
            },
            {
                _name: 'CSidebarNavTitle',
                _children: ['exam management']
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Exam List',
                to: '/mfu/exam',
                icon: 'cil-list-numbered'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Exam Set',
                to: '/mfu/examset',
                icon: 'cil-copy'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Exam Analysis',
                to: '/mfu/analysis',
                icon: 'cil-calculator'
            },
            {
                _name: 'CSidebarNavTitle',
                _children: ['course configuration']
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Course',
                to: '/mfu/course',
                icon: 'cil-book'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Topic',
                to: '/mfu/topic',
                icon: 'cil-description'
            },
            // {
            //   _name: 'CSidebarNavItem',
            //   name: 'Question Type',
            //   to: '/admin/questionType',
            //   icon: 'cil-text'
            // },
            {
                _name: 'CSidebarNavTitle',
                _children: ['file management']
            },
            {
                _name: 'CSidebarNavItem',
                name: 'File Transfer',
                to: '/mfu/file',
                icon: 'cil-envelope-closed'
            },
            {
                _name: 'CSidebarNavTitle',
                _children: ['user management']
            },
            {
                _name: 'CSidebarNavItem',
                name: 'User',
                to: '/mfu/user',
                icon: 'cil-user'
            },
            {
                _name: 'CSidebarNavTitle',
                _children: ['authentication']
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Change Password',
                to: '/mfu/changepassword',
                icon: 'cil-lock-locked'
            },
            {
                _name: 'CSidebarNavItem',
                name: 'Log out',
                to: '/login',
                icon: 'cil-account-logout'
            }
        ]
    }
]

import Vue from 'vue'
import Router from 'vue-router'

// Containers
const TheContainer = () => import('@/containers/TheContainer')

// Views
const Dashboard = () => import('@/views/Dashboard')

// Views - Pages
const Login = () => import('@/views/Login')
const Logout = () => import('@/views/Logout')
const ChangePassword = () => import('@/views/ChangePassword')

// Views - Item Banking System
const Course = () => import('@/views/MainCourse')
const Topic = () => import('@/views/MainTopic')
const Exam = () => import('@/views/MainExam')
const ExamSet = () => import('@/views/MainExamSet')
const ExamAnalysis = () => import('@/views/MainExamAnalysis')
const Teacher = () => import('@/views/MainTeacher')
const File = () => import('@/views/MainFile')
const AnalyticReport = () => import("@/views/MainAnalyticReport")
const QuestionType = () => import("@/views/MainQuestionType")
const Formula = () => import("@/views/MainFormula")

Vue.use(Router)

const routerModule = new Router({
    mode: 'hash', // https://router.vuejs.org/api/#mode
    linkActiveClass: 'active',
    scrollBehavior: () => ({y: 0}),
    routes: configRoutes()
})

function configRoutes() {
    return [
        {
            path: '/',
            redirect: '/login',
            name: 'Authentication',
            component: {
                render(c) {
                    return c('router-view')
                }
            },
            children: [
                {
                    path: 'login',
                    name: 'Login',
                    component: Login
                },
                {
                    path: 'logout',
                    name: 'Logout',
                    component: Logout
                }
            ]
        },
        {
            path: "/mfu",
            redirect: "/dashboard",
            name: "Admin",
            component: TheContainer,
            children: [
                {
                    path: '/mfu/dashboard',
                    name: 'Main Page',
                    component: Dashboard
                },
                {
                    path: '/mfu/formula',
                    name: 'Formula',
                    component: Formula
                },
                {
                    path: '/mfu/course',
                    name: 'Course',
                    component: Course
                },
                {
                    path: '/mfu/topic',
                    name: 'Topic',
                    component: Topic
                },
                {
                    path: '/mfu/exam',
                    name: 'Exam',
                    component: Exam
                },
                {
                    path: '/mfu/examset',
                    name: 'Exam Set',
                    component: ExamSet
                },
                {
                    path: '/mfu/analysis',
                    name: 'Exam Analysis',
                    component: ExamAnalysis
                },
                {
                    path: '/mfu/user',
                    name: 'User',
                    component: Teacher
                },
                {
                    path: '/mfu/changepassword',
                    name: 'Change Password',
                    component: ChangePassword
                },
                {
                    path: '/mfu/file',
                    name: 'File',
                    component: File
                },
                {
                    path: '/mfu/report',
                    name: 'AnalyticReport',
                    component: AnalyticReport
                },
                {
                    path: '/mfu/questionType',
                    name: 'QuestionType',
                    component: QuestionType
                },
            ]
        }
    ]
}

export default routerModule


import router from "../router/index"

export default {
    errorHandler(code){
        switch (+code){
            case 401:
                router.push("/").catch(()=>{});
                break
            default:
                console.log(code)
        }
    }
}

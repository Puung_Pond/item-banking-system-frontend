import axios from 'axios';

const instance = axios.create();

instance.defaults.baseURL = 'http://10.1.104.205:3000/api/v1/';
// instance.defaults.baseURL = 'https://item-banking.herokuapp.com/api/v1/';

// instance.defaults.timeout = 10000;

instance.defaults.headers = {
    "Content-Type": "application/json",
}

export default {
    // Authorization
    onSetHeader(body) {
        if (body.form) {
            instance.defaults.headers = {
                // "Content-Type": "multipart/form-data",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        } else {
            instance.defaults.headers = {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        }
    },
    onClearHeader() {
        instance.defaults.headers = ""
    },
    onLogin(body) {
        return instance.post("auth/login", body)
    },
    onUpdatePassword(body) {
        this.onSetHeader({form: false})
        return instance.put("auth/updatepassword", body)
    },
    onLogout() {
        this.onClearHeader()
        return instance.post("auth/logout")
    },

    // User information
    onFetchUserInformation() {
        this.onSetHeader({form: false})
        return instance.get("auth/me")
    },

    // Dashboard
    onFetchDashboard() {
        this.onSetHeader({form: false})
        return instance.get("dashboard")
    },

    //QuestionType
    onFetchAllQuestionTypes() {
        this.onSetHeader({form: false})
        return instance.get("questiontypes")
    },
    onCreateQuestionType(body) {
        this.onSetHeader({form: false})
        return instance.post("questiontypes", body)
    },
    onEditQuestionType(questionTypeId, body) {
        this.onSetHeader({form: false})
        return instance.put("questiontypes/" + questionTypeId, body)
    },
    onDeleteQuestionType(id) {
        this.onSetHeader({form: false})
        return instance.delete("questiontypes/" + id)
    },

    //AnalyticReports
    onFetchAllAnalyticReports() {
        this.onSetHeader({form: false})
        return instance.get("analysis")
    },
    onFetchSingleAnalyticReports(id) {
        this.onSetHeader({form: false})
        return instance.get("analysis/" + id)
    },
    onDeleteAnalyticReports(id) {
        this.onSetHeader({form: false})
        return instance.delete("analysis/" + id)
    },

    // Courses
    onFetchAllCourses() {
        this.onSetHeader({form: false})
        return instance.get("courses")
    },
    onFetchSingleCourse(courseId) {
        this.onSetHeader({form: false})
        return instance.get("courses/" + courseId)
    },
    onCreateCourse(body) {
        this.onSetHeader({form: false})
        return instance.post("courses", body)
    },
    onEditCourse(courseId, body) {
        this.onSetHeader({form: false})
        return instance.put("courses/" + courseId, body)
    },
    onDeleteCourse(courseId) {
        this.onSetHeader({form: false})
        return instance.delete("courses/" + courseId)
    },

    // Topics
    onFetchAllTopic() {
        this.onSetHeader({form: false})
        return instance.get("topics")
    },
    onCreateTopic(body) {
        this.onSetHeader({form: false})
        return instance.post("courses/" + body.courseId + "/topics", {name: body.name})
    },
    onEditTopic(topicId, body) {
        this.onSetHeader({form: false})
        return instance.put("topics/" + topicId, body)
    },
    onDeleteTopic(topicId) {
        this.onSetHeader({form: false})
        return instance.delete("topics/" + topicId)
    },

    // Teachers
    onFetchAllUser() {
        this.onSetHeader({form: false})
        return instance.get("users")
    },
    onCreateUser(body) {
        this.onSetHeader({form: false})
        return instance.post("users/", body)
    },
    onEditUser(body) {
        this.onSetHeader({form: false})
        return instance.put("users/" + body.userId, body.body)
    },
    onDeleteUser(userId) {
        this.onSetHeader({form: false})
        return instance.delete("users/" + userId)
    },
    onResetUserPassword(body) {
        this.onSetHeader({form: false})
        return instance.put("users/resetpassword/" + body.userId, {newPassword: body.newPassword})
    },

    // Exam
    onFetchAllExam() {
        this.onSetHeader({form: false})
        return instance.get("exams")
    },
    onFetchSingleExamSet(url) {
        this.onSetHeader({form: false})
        return instance.get(`exams/${url}`)
    },
    onRandomExamByTOS(body) {
        this.onSetHeader({form: false})
        return instance.post("exams/tos", {name: body.name, tos: body.tos})
    },
    onAddExam(body) {
        this.onSetHeader({form: true})
        return instance.post("exams", body)
    },

    // ExamSet
    onFetchAllExamSet() {
        this.onSetHeader({form: false})
        return instance.get("examsets")
    },
    onGetSingleExamset(id) {
        this.onSetHeader({form: false})
        return instance.get(`examsets/${id}`)
    },
    onExamsetAnalysisThenSave(body) {
        this.onSetHeader({form: true})
        return instance.put("examsets/analyze/", body)
    },
    onExamsetAnalysisThenNotSave(body) {
        this.onSetHeader({form: true})
        return instance.post("examsets/analyze/", body)
    },
    onDeleteExamset(examSetId) {
        this.onSetHeader({form: false})
        return instance.delete("examsets/" + examSetId)
    },

    // File
    onFetchAllFile() {
        this.onSetHeader({form: false})
        return instance.get("files")
    },
    onAcceptFile(id, body) {
        this.onSetHeader({form: false})
        return instance.put("files/" + id, body)
    },
    onSendFile(body) {
        this.onSetHeader({form: true})
        return instance.post("files/", body)
    },
    onDeleteFile(body) {
        this.onSetHeader({form: false})
        return instance.delete("files/" + body)
    }
}
